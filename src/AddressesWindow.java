import java.util.Calendar;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class AddressesWindow extends Stage {
	
	private TableView<Address> table = new TableView<Address>();
	private Stage primaryStage;
	Address selected;
	List<OrderLine> orderlineList;
	
	public void setData(ObservableList<Address> addresses) {
		table.setItems(addresses);
	}
	
	@SuppressWarnings("unchecked")
	public AddressesWindow(Stage ps, List<OrderLine> orderlineList) {
		this.primaryStage = ps;
		this.orderlineList = orderlineList;
		setData(DefaultData.addresses);
		setTitle("Addresses List");
		final Label label = new Label("Select Address");
        label.setFont(new Font("Arial", 16));
        HBox labelHbox = new HBox(10);
        labelHbox.setAlignment(Pos.CENTER);
        labelHbox.getChildren().add(label);

        table.setEditable(true);
       
        TableColumn StreetCol = new TableColumn("Street");
        StreetCol.setMinWidth(200);
        StreetCol.setCellValueFactory(new PropertyValueFactory<Address,String>("Street"));
        StreetCol.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn CityCol = new TableColumn("City");
        CityCol.setCellValueFactory(new PropertyValueFactory<Address,Calendar>("City"));
        CityCol.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn StateCol = new TableColumn("State");
        StateCol.setCellValueFactory(new PropertyValueFactory<Address,String>("State"));
        StateCol.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn ZipCol = new TableColumn("Zip");
        ZipCol.setCellValueFactory(new PropertyValueFactory<Address,String>("Zip"));
        ZipCol.setCellFactory(TextFieldTableCell.forTableColumn());
        
        table.getColumns().addAll(StreetCol, CityCol , StateCol , ZipCol);
        
        Button okButton = new Button("Ok");
		Button backButton = new Button("Back");
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		grid.add(labelHbox, 0, 0);
		grid.add(table, 0, 1);
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.getChildren().add(okButton);
		btnBox.getChildren().add(backButton);
		grid.add(btnBox,0,3);

		backButton.setOnAction(evt -> {
			new ShippingBilling(primaryStage, orderlineList, new Address()).show();
			hide();
		});

		okButton.setOnAction(evt -> {
			selected = table.getSelectionModel().getSelectedItem();
			ShippingBilling shippingBilling = new ShippingBilling(primaryStage, orderlineList, selected);
			hide();
			shippingBilling.show();
		});
   
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
