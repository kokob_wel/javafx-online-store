import java.text.SimpleDateFormat;
import java.util.Date;

public class Book {
	
	private String title;
	private String publishDate;
	private double price;
	private int amount;

	public Book(String title, double price,Date date, int amount) {
		super();
		this.title = title;
		this.publishDate =(new SimpleDateFormat("yyyy-MM-dd")).format(date);
		this.price = price;
		this.amount = amount;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
