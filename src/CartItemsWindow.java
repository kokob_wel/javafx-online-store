import java.util.*;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

public class CartItemsWindow extends Stage {
	Stage primaryStage;
	private final TableView<OrderLine> table = new TableView<OrderLine>();
	List<OrderLine> orderLineList;
	
	private ObservableList<OrderLine> observableOrderList;
	
	CartItemsWindow(Stage ps, List<OrderLine> list) {
		this.orderLineList = list;
		primaryStage = ps;
		this.observableOrderList =  FXCollections.observableArrayList(list);
		setTitle("Cart Items");
		
		Text itemsOnCart = new Text();
		itemsOnCart.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 25));
		HBox itemTotalBox = new HBox();
		itemTotalBox.setAlignment(Pos.BOTTOM_RIGHT);
		itemTotalBox.setPadding(new Insets(8));
		
		final Label titleLabel = new Label("Cart Items");
		titleLabel.setPadding(new Insets(20));
		titleLabel.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 15));
		
		double sumTotal=0.0;
		for(OrderLine order : this.orderLineList) {
			sumTotal += order.getTotalPrice();
		}
		
		itemsOnCart.setText("Total: $" + sumTotal);
		itemTotalBox.getChildren().add(itemsOnCart);
		
		Button proceedBtn = new Button("Proceed To Checkout");
		Button continueShoppingBtn = new Button("Continue Shopping");
		Button saveBtn = new Button("Save To Cart");
		Button exitBtn = new Button("Exit E-Bazaar");
		proceedBtn.getStyleClass().add("button");
		continueShoppingBtn.getStyleClass().add("button");
		saveBtn.getStyleClass().add("button");
		exitBtn.getStyleClass().add("button");
		Text messageLabel = new Text();
		messageLabel.setFont(Font.font("Tahoma", FontWeight.MEDIUM, 12));
		messageLabel.setFill(Color.GREEN);
		
		table.setEditable(true);
		
		TableColumn item = new TableColumn("Item");
		item.setMinWidth(200);
		item.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("itemName"));
        
		TableColumn quantity = new TableColumn("Quantity");
		quantity.setMinWidth(100);
		quantity.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("quantity"));
		
		TableColumn unitPrice = new TableColumn("Unit Price");
		unitPrice.setMinWidth(100);
		unitPrice.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("unitPrice"));
		
		TableColumn total = new TableColumn("Total");
		total.setMinWidth(100);
		total.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("totalPrice"));
		
		TableColumn description = new TableColumn("Description");
		description.setMinWidth(300);
		description.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("description"));
		
		table.getColumns().addAll(item, quantity, unitPrice, total, description);
		
		table.setItems(this.observableOrderList);
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		HBox titleBox = new HBox(10);
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setPadding(new Insets(10));
		titleBox.getChildren().add(titleLabel);
		grid.add(titleBox, 0, 0);
		grid.add(table, 0, 1);
		
		HBox outputMessageBox = new HBox();
		outputMessageBox.setAlignment(Pos.CENTER);
		outputMessageBox.getChildren().add(messageLabel);
		grid.add(outputMessageBox, 0, 3);
		grid.add(itemTotalBox, 0, 4);
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.setSpacing(20);
		btnBox.setPadding(new Insets(15));
		btnBox.getChildren().add(proceedBtn);
		btnBox.getChildren().add(continueShoppingBtn);
		btnBox.getChildren().add(saveBtn);
		btnBox.getChildren().add(exitBtn);
		grid.add(btnBox,0,5);

		proceedBtn.setOnAction(evt-> {
			ShippingBilling shippingBillingWindow = new ShippingBilling(primaryStage, this.orderLineList, new Address());
			hide();
			shippingBillingWindow.show();
		});
		
		continueShoppingBtn.setOnAction(evt-> {
			CatalogListWindow catalogs = new CatalogListWindow(primaryStage, this.orderLineList);
			catalogs.setData(DefaultData.CATALOG_LIST_DATA);
			hide();
			catalogs.show();
		});
		
		saveBtn.setOnAction(evt -> {
			messageLabel.setText(this.orderLineList.size() + " item (s) successfully saved in your cart !!!");
		});
		
		exitBtn.setOnAction(evt -> {
			Platform.exit();
		});
		
		Scene scene = new Scene(grid,850, 600);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
