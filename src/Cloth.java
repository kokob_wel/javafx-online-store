import java.text.SimpleDateFormat;
import java.util.Date;

public class Cloth {
	
	private String clothType;
	private double price;
	private String mfgDate;
	private double amount;
	
	public Cloth(String clothType, double price, Date date, double amount) {
		super();
		this.clothType = clothType;
		this.price = price;
		this.mfgDate = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
		this.amount = amount;
	}
	public String getClothType() {
		return clothType;
	}
	public void setClothType(String clothType) {
		this.clothType = clothType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getMfgDate() {
		return mfgDate;
	}
	public void setMfgDate(String mfgDate) {
		this.mfgDate = mfgDate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
