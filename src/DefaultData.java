import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

public class DefaultData {
	
	 public static final ObservableList<Order> orders =
		        FXCollections.observableArrayList(
		          new Order("XB13425",new Date(), 300),
		          new Order("ZZ13425",new Date(), 400),
		          new Order("BB13425",new Date(), 500)
		         
		        );

	 public static final ObservableList<Address> addresses =
		        FXCollections.observableArrayList(
		          new Address("1607 Granville Ave","Fairfield","IA","52556"),
		          new Address("1000N 4th street","Fairfield","IA","52557"),
		          new Address("200 Burlington Ave","Fairfield","IA","52556")
		         
		        ); 
		
	 public static final ObservableList<Book> Books =
		        FXCollections.observableArrayList(
		          new Book("Gone with the Wind",12.00,new Date(),20),
		          new Book("Messiah of Dune",43.00,new Date(),100),
		          new Book("Garden of Rama",52.00,new Date(),30)
		         
		        );
	 
	 public static final ObservableList<Cloth> clothes =
		        FXCollections.observableArrayList(
		          new Cloth("pants",12.00,new Date(),20),
		          new Cloth("Skirts",43.00,new Date(),100),
		          new Cloth("T-Shirts",52.00,new Date(),30)
		         
		        );

	public static final String USERNAME ="group3";
	public static final String PASSWORD ="group3";
	public static final Catalog BOOKS_CATALOG = new Catalog("Books");
	public static final Catalog CLOTHES_CATALOG = new Catalog("Clothes");
	public static final Product MESSIAH_BOOK = new Product("Messiah Of Dune", "11/11/2000", 20, 15.00);
	public static final Product GONE_BOOK = new Product("Gone with the Wind", "12/5/1995", 15, 12.00);
	public static final Product GARDEN_BOOK = new Product("Garden of Rama", "1/1/2005", 5, 18.00);
	public static final Product PANTS = new Product("Pants", "11/1/2000", 20, 15.00);
	public static final Product SKIRTS = new Product("Skirts", "1/5/1995", 15, 12.00);
	public static final Product TSHIRTS = new Product("T-Shirts", "1/10/2005", 10, 22.00);
	public static final ObservableList<Catalog> CATALOG_LIST_DATA =
            FXCollections.observableArrayList(
            BOOKS_CATALOG, CLOTHES_CATALOG);
	public final static ObservableMap<Catalog, List<Product>> PRODUCT_LIST_DATA =
            FXCollections.observableHashMap();
	public static final List<String> DISPLAY_PRODUCT_FIELDS 
	   = Arrays.asList("Item Name", "Price", "Quantity Available", "Review");
	static {
		MESSIAH_BOOK.setDescription("You saw how good Dune was. \nThis is Part 2 of this \nunforgettable trilogy.");
		GONE_BOOK.setDescription("A moving classic that tells \na tale of love and \na tale of courage.");
		GARDEN_BOOK.setDescription("Highly acclaimed Book \nof Isaac Asimov. A real \nnail-biter.");
		PANTS.setDescription("I've seen the Grand Canyon. \nI've camped at Yellowstone. But nothing \non earth compares to the look and feel of \nthese pants.");
		SKIRTS.setDescription("Once this brand of skirts \nbecomes well-known, watch out!");
		TSHIRTS.setDescription("Can be worn by men or women. \nAlways in style.");
		
		PRODUCT_LIST_DATA.put(BOOKS_CATALOG, 
				Arrays.asList(MESSIAH_BOOK, GONE_BOOK, GARDEN_BOOK));
						
		PRODUCT_LIST_DATA.put(CLOTHES_CATALOG, 
				Arrays.asList(PANTS, SKIRTS, TSHIRTS));
	}
}