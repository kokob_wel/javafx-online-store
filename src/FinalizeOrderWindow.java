import java.util.*;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

public class FinalizeOrderWindow extends Stage {
	private Stage primaryStage;
	private final TableView<OrderLine> table = new TableView<OrderLine>();
	List<OrderLine> orderLineList;
	
	private ObservableList<OrderLine> observableOrderList;
	
	FinalizeOrderWindow(Stage ps, List<OrderLine> list) {
		this.orderLineList = list;
		this.primaryStage = ps;
		this.observableOrderList =  FXCollections.observableArrayList(list);
		setTitle("Final Order");
		
		final Label titleLabel = new Label("Final Order");
		titleLabel.setFont(new Font("Arial", 20));
		
		Button submitBtn = new Button("Submit Order");
		Button cancelBtn = new Button("Cancel");

		table.setEditable(true);
		TableColumn item = new TableColumn("Item");
		item.setMinWidth(200);
		item.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("itemName"));

		TableColumn quantity = new TableColumn("Quantity");
		quantity.setMinWidth(100);
		quantity.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("quantity"));
		
		TableColumn unitPrice = new TableColumn("Unit Price");
		unitPrice.setMinWidth(100);
		unitPrice.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("unitPrice"));
		
		TableColumn total = new TableColumn("Total");
		total.setMinWidth(100);
		total.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("totalPrice"));
		
		TableColumn description = new TableColumn("Description");
		description.setMinWidth(300);
		description.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("description"));
		
		table.getColumns().addAll(item, quantity, unitPrice, total, description);
		table.setItems(this.observableOrderList);
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		HBox titleBox = new HBox(10);
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setPadding(new Insets(10));
		titleBox.getChildren().add(titleLabel);
		grid.add(titleBox, 0, 0);
		grid.add(table, 0, 1);
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.setSpacing(20);
		btnBox.getChildren().add(submitBtn);
		btnBox.getChildren().add(cancelBtn);

		grid.add(btnBox,0,3);
		
		submitBtn.setOnAction(e-> {			
	        ThankYouWindow thankyou = new ThankYouWindow(primaryStage);
	        thankyou.show();	
	        hide();
	    
	});
		cancelBtn.setOnAction(evt -> Platform.exit());
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
