import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Login extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX Welcome");
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Image image = new Image("logo2.jpg");
		ImageView footerView = new ImageView();
		footerView.setImage(image); 

		HBox footerHb = new HBox(10);
		footerHb.setAlignment(Pos.BOTTOM_LEFT);
		footerHb.getChildren().add(footerView);    	
		grid.add(footerHb, 0, 0);

		Text scenetitle = new Text("Please Sign In Below:");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		scenetitle.setFill(Color.DARKRED);
		HBox titleBox = new HBox();
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setSpacing(15);
		titleBox.setPadding(new Insets(14));
		titleBox.getChildren().add(scenetitle);
		grid.add(titleBox, 0, 1);

		Label userName = new Label("User Name:");
		userName.setAlignment(Pos.CENTER);
		grid.add(userName, 0, 2);

		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 2);

		Label pw = new Label("Password:");
		grid.add(pw, 0, 3);

		PasswordField pwBox = new PasswordField();
		grid.add(pwBox, 1, 3);

		Button btn = new Button("Sign in");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 5);

		btn.setOnAction(e-> {
			Start start = new Start(primaryStage);
			//		        start.show();
		});
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		primaryStage.setScene(scene);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());;
		primaryStage.show();
	}
}