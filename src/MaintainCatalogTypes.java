import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MaintainCatalogTypes extends Stage {

	private TableView<Catalog> table = new TableView<Catalog>();
	Stage primaryStage;
	Catalog selected;
	
	public void setData(ObservableList<Catalog> cats) {
		table.setItems(cats);
	}
	@SuppressWarnings("unchecked")
	public MaintainCatalogTypes(Stage ps) {
		primaryStage = ps;
		setTitle("Maintain catalog List");
		
        HBox labelHbox = new HBox(10);
        labelHbox.setAlignment(Pos.CENTER);
		
		TableColumn<Catalog, String> catalogNameCol = new TableColumn<>("Catalog");
		catalogNameCol.setMinWidth(250);
		catalogNameCol.setCellValueFactory(
            new PropertyValueFactory<Catalog, String>("name"));
		catalogNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		
		table.getColumns().addAll(catalogNameCol);
		table.setItems((new DefaultData()).CATALOG_LIST_DATA);
		
		Button editButton = new Button("Edit Catalog");
		Button backButton = new Button("Back to Start");
		Button addButton = new Button("Add catalog");
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		grid.add(labelHbox, 0, 0);
		grid.add(table, 0, 1);
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.getChildren().add(editButton);
		btnBox.getChildren().add(backButton);
		btnBox.getChildren().add(addButton);
		grid.add(btnBox,0,3);
        
		backButton.setOnAction(evt -> {
			primaryStage.show();
			hide();
		});

		final Text actiontarget = new Text();
        grid.add(actiontarget, 0, 5);
        actiontarget.setId("actiontarget");

        addButton.setOnAction(e->{
                actiontarget.setText("Code needs to be added");            
        });
        
        editButton.setOnAction(e->{
                actiontarget.setText("Code needs to be added");
            
        });

		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
