import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Order {

    private String orderItem;
	private String orderDdate;
	private double price;
	
	public Order(String orderItem, Date date, double price) {
		super();
		this.orderItem = orderItem;
		this.orderDdate = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
		this.price = price;
	}
	
	public String getOrderItem() {
		return orderItem;
	}
	public void setOrderItem(String orderItem) {
		this.orderItem = orderItem;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDate() {
		return orderDdate;
	}
	public String getOrderDdate() {
		return orderDdate;
	}
	public void setOrderDdate(String orderDdate) {
		this.orderDdate = orderDdate;
	}
}