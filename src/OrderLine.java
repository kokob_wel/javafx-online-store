
public class OrderLine {

	private String itemName;
	private double quantity;
	private double unitPrice;
	private String description;
	
	public OrderLine() {
		
	}
	
	public OrderLine(String itemName, double quantity, double unitPrice , String description) {
		this.itemName = itemName;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.description = description;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getTotalPrice() {
		return quantity*unitPrice;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}

	@Override
	public String toString() {
		return "OrderLine [itemName=" + itemName + ", quantity=" + quantity
				+ ", unitPrice=" + unitPrice + ", totalPrice=" + getTotalPrice()
				+ "]";
	}
}