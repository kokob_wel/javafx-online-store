import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class PaymentWindow extends Stage {
	
	private Stage primaryStage;
	private List<OrderLine> orderlineList;
	
	public PaymentWindow(Stage ps , List<OrderLine> list) {
		 
		this.primaryStage = ps;
		this.orderlineList = list;
		
		primaryStage.setTitle("Payment Window");
		 
		 VBox vbMain = new VBox(20);
		 
		 vbMain.setAlignment(Pos.CENTER);
		 vbMain.setPadding(new Insets(25, 25, 25, 25));
		 Label MAIN_LABEL = new Label("Payment Method");
		 vbMain.getChildren().add(MAIN_LABEL);
		 
		 GridPane grid2 = new GridPane();
	     grid2.setAlignment(Pos.CENTER);
	     grid2.setHgap(10);
	     grid2.setVgap(5);
	     grid2.setPadding(new Insets(10, 10, 10, 10));
	     
	        Label Name = new Label("Name on Card ");
		 	grid2.add(Name, 0, 1);

		 	TextField nameTextField = new TextField();
		 	grid2.add(nameTextField, 1, 1);
		 	
		 	Label address1 = new Label("Card Number ");
		 	grid2.add(address1, 0, 2);

		 	TextField address1TextField = new TextField();
		 	grid2.add(address1TextField, 1, 2);
		 	
		 	Label city = new Label("Card Type ");
		 	grid2.add(city, 0, 3);

		 	ComboBox<String> cartType = new ComboBox<String>();
	        cartType.getItems().addAll("Visa","Master Card","Discover");
		 	
	        grid2.add(cartType, 1, 3);
		 	
		 	Label state = new Label("Expiration Date ");
		 	grid2.add(state, 0, 4);

		 	TextField stateTextField = new TextField();
		 	grid2.add(stateTextField, 1, 4);	
		 	
		 vbMain.getChildren().add(grid2);
		 
		 HBox bottomButtons = new HBox();
		 
		 bottomButtons.setAlignment(Pos.BOTTOM_CENTER);
		 bottomButtons.setSpacing(10);
		 
		 Button proceedWithChkOut = new Button("Proceed With Checkout");
		 proceedWithChkOut.setEffect(new Reflection());

		 Button backToCart = new Button("Back To Cart");
		 backToCart.setEffect(new Reflection());
		 
		 bottomButtons.getChildren().add(proceedWithChkOut);
		 bottomButtons.getChildren().add(backToCart);
		 
		 vbMain.getChildren().add(bottomButtons);
		 
		 proceedWithChkOut.setOnAction(evt -> {
				TermsWindow termsWin = new TermsWindow(primaryStage ,orderlineList);
				hide();			
				termsWin.show();
			});
		 
		   Scene scene = new Scene(vbMain,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		   scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		   setScene(scene);
	}
}
