import java.util.Calendar;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ProductCatalog extends Stage {
	private TableView<Book> tableBook = new TableView<Book>();
	private TableView<Cloth> tableCloth = new TableView<Cloth>();
	Stage primaryStage;
	Catalog selected;
	
	public void setBookData(ObservableList<Book> list) {
		tableBook.setItems(list);
	}
	public void setClothesData(ObservableList<Cloth> list) {
		tableCloth.setItems(list);
	}
	@SuppressWarnings("unchecked")
	public ProductCatalog(Stage ps) {
		primaryStage = ps;
		setTitle("Catalog List");
		final Label label = new Label("Maintain Product Catalog");
        label.setFont(new Font("Arial", 16));
        final Label labelSelect = new Label("Choose Product");
        HBox labelHbox = new HBox(10);
        labelHbox.setAlignment(Pos.CENTER);
        labelHbox.getChildren().add(label);
        
        HBox labelHbox2 = new HBox(10);
        labelHbox2.setAlignment(Pos.BASELINE_LEFT);
        labelHbox2.getChildren().add(labelSelect);

        ObservableList<String> options = 
        	    FXCollections.observableArrayList(
        	        "Books",
        	        "Clothes"
        	    );
        	final ComboBox comboBox = new ComboBox(options);
        	comboBox.setValue("Books");
       
        TableColumn BookTitleCol = new TableColumn("Book Title");
        BookTitleCol.setCellValueFactory(new PropertyValueFactory<Order,String>("title"));
        TableColumn publishDateCol = new TableColumn("Publish Date");
        publishDateCol.setCellValueFactory(new PropertyValueFactory<Order,Calendar>("publishDate"));
        TableColumn priceCol = new TableColumn("Total Price");       
        priceCol.setCellValueFactory(new PropertyValueFactory<Order,String>("price"));        
        TableColumn amountCol = new TableColumn("Available amount");
        amountCol.setMinWidth(200);
        amountCol.setCellValueFactory(new PropertyValueFactory<Order,String>("amount")
        );

        TableColumn clothTypeCol = new TableColumn("Cloth Type");
        clothTypeCol.setCellValueFactory(new PropertyValueFactory<Order,String>("clothType"));
        TableColumn priceClothCol = new TableColumn("Price");
        priceClothCol.setCellValueFactory(new PropertyValueFactory<Order,Calendar>("price"));
        TableColumn mfgDateCol = new TableColumn("Year made");       
        mfgDateCol.setCellValueFactory(new PropertyValueFactory<Order,String>("mfgDate"));        
        TableColumn amountCLothCol = new TableColumn("Available amount");
        amountCLothCol.setMinWidth(200);
        amountCLothCol.setCellValueFactory(new PropertyValueFactory<Order,String>("amount")
        );

        tableBook.getColumns().addAll(BookTitleCol, publishDateCol, priceCol,amountCol);
        tableCloth.getColumns().addAll(clothTypeCol, priceClothCol, mfgDateCol,amountCLothCol);
		
		Button addButton = new Button("Add");
		Button editButton = new Button("edit");
		Button backButton = new Button("Cancel");
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		
		grid.add(labelHbox, 0, 0);
		grid.add(labelHbox2, 0, 1);
		grid.add(comboBox, 0, 2);

		grid.add(tableBook, 0, 3);
		
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.getChildren().add(addButton);
		btnBox.getChildren().add(editButton);
		btnBox.getChildren().add(backButton);
		
		grid.add(btnBox,0,4);
		backButton.setOnAction(evt -> {
			primaryStage.show();
			hide();
		});

		addButton.setOnAction(evt -> {
		});
		
		comboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                switch (t1.toString()) {
                    case "Books":
//                    	grid.getChildren().clear();
                    	grid.getChildren().remove(tableBook);
                    	grid.add(tableBook, 0, 3);
                    	show();
                        break;
                   case "Clothes":
//                	   grid.getChildren().clear(); 
                	   grid.getChildren().remove(tableCloth);
                	   grid.add(tableCloth, 0, 3);
                		show();                		
                        break;
                }
            }
        });
		
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT); 
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}