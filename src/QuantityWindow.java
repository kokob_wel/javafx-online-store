import java.util.*;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class QuantityWindow extends Stage {
	Stage primaryStage;
	private OrderLine orderline;
	private List<OrderLine> orderLineList;
	
	@SuppressWarnings("unchecked")
	QuantityWindow(Stage ps, OrderLine orderline, List<OrderLine> orderLineList) {
		primaryStage = ps;
		this.orderline = orderline;
		this.orderLineList = orderLineList;
		primaryStage.setTitle("Quantity Desired");
		
		Label labelTitle = new Label("Quantity Desired");
		labelTitle.setFont(new Font("Arial", 20));
		labelTitle.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 15));
		//labelTitle.setFill(Color.DARKRED);
		
		TextField quantityTextField = new TextField();
		Button OkBtn = new Button("OK");
		Button cancelBtn = new Button("Cancel");
		Text errorMessage = new Text();
		errorMessage.setFont(Font.font("Tahoma", FontWeight.MEDIUM, 12));
		errorMessage.setFill(Color.RED);
		
		VBox vBox = new VBox(10);
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setSpacing(5);
		vBox.setPadding(new Insets(5));
        final ComboBox quantity = new ComboBox();
        HBox hb = new HBox(10);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().add(quantityTextField);
        hb.getChildren().add(quantity);
        quantity.getItems().addAll(
            "1","2", "3","4","5","6","7","8","9" ,"10" 
        );
		vBox.getChildren().add(labelTitle);
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
        grid.add(vBox, 0, 0);
        
        HBox errBox = new HBox();
        errBox.setAlignment(Pos.CENTER);
        errBox.getChildren().add(errorMessage);
        
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER);
		hBox.setSpacing(10);
		hBox.getChildren().add(OkBtn);
		hBox.getChildren().add(cancelBtn);
		hBox.setPadding(new Insets(8));
		
		VBox vB = new VBox();
		vB.setSpacing(8);
		vB.setAlignment(Pos.CENTER);
		vB.getChildren().add(hb);
		vB.getChildren().add(hBox);
		grid.add(vB, 0, 2);
		grid.add(errBox, 0, 3);
		
		OkBtn.setOnAction(evt -> {
			String quantitySelected = "";
			if(quantity.getValue() == null && quantityTextField.getText().isEmpty()) {
				errorMessage.setText("Please provide us with a number");
			}else if(quantity.getValue() != null){
				quantitySelected = quantity.getValue().toString();
			}else {
				quantitySelected = quantityTextField.getText();
			}

			this.orderline.setQuantity(Double.parseDouble(quantitySelected));
			double addedQuantity = this.orderline.getQuantity();
			boolean flag = false;
			if(!this.orderLineList.isEmpty()) {
				for(OrderLine order : this.orderLineList) {
					if(order.getItemName().equals(this.orderline.getItemName())) {
						order.setQuantity(order.getQuantity() + addedQuantity);
						flag= true;
						break;
					}
				}
			}
			if(flag == false) this.orderLineList.add(this.orderline);
			CartItemsWindow cartItem = new CartItemsWindow(this, this.orderLineList);			
			hide();
			cartItem.show();
		});
		
		cancelBtn.setOnAction(evt -> {
			Platform.exit();
		});
		
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT); 
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
