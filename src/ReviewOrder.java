import java.util.Calendar;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ReviewOrder extends Stage {
	private TableView<Order> table = new TableView<Order>();
	Stage primaryStage;
	Catalog selected;

	public void setData(ObservableList<Order> list) {
		table.setItems(list);
	}
	@SuppressWarnings("unchecked")
	public ReviewOrder(Stage ps) {
		primaryStage = ps;
		setTitle("Catalog List");

		final Label label = new Label("Review Orders");
		label.setFont(new Font("Arial", 16));
		HBox labelHbox = new HBox(10);
		labelHbox.setAlignment(Pos.CENTER);
		labelHbox.getChildren().add(label);

		table.setEditable(true);

		TableColumn orderItemCol = new TableColumn("Order Item");
		orderItemCol.setCellValueFactory(new PropertyValueFactory<Order,String>("orderItem"));
		orderItemCol.setMinWidth(200);

		TableColumn orderDateCol = new TableColumn("Order Date");
		orderDateCol.setCellValueFactory(new PropertyValueFactory<Order,Calendar>("date"));
		orderDateCol.setMinWidth(200);

		TableColumn priceCol = new TableColumn("Total Price");
		priceCol.setMinWidth(200);
		priceCol.setCellValueFactory(new PropertyValueFactory<Order,String>("price"));
		priceCol.setMinWidth(200);

		table.getColumns().addAll(orderItemCol, orderDateCol, priceCol);

		Button viewButton = new Button("View Details");
		Button backButton = new Button("Cancel");

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		grid.add(labelHbox, 0, 0);
		grid.add(table, 0, 1);
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.getChildren().add(viewButton);
		btnBox.getChildren().add(backButton);
		grid.add(btnBox,0,3);

		backButton.setOnAction(evt -> {
			primaryStage.show();
			hide();
		});

		final Text actiontarget = new Text();
		grid.add(actiontarget, 0, 5);
		actiontarget.setId("actiontarget");

		viewButton.setOnAction(e->{
			actiontarget.setText("Code needs to be added");            
		});
		Scene scene = new Scene(grid,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}
