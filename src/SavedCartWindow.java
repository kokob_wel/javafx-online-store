import java.util.List;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SavedCartWindow extends Stage {
	Stage primaryStage;
	private final TableView<OrderLine> table = new TableView<OrderLine>();
	List<OrderLine> orderLineList;
	
	private ObservableList<OrderLine> observableOrderList;
	
	SavedCartWindow(Stage ps, List<OrderLine> list) {
		this.orderLineList = list;
		primaryStage = ps;
		//list.stream().forEach(System.out::println);
		this.observableOrderList =  FXCollections.observableArrayList(list);
		//System.out.println(this.observableOrderList);
		setTitle("Cart Items");
		final Label titleLabel = new Label("Cart Items");
		titleLabel.setFont(new Font("Arial", 20));
		
		Button proceedBtn = new Button("Proceed To Checkout");
		Button continueShoppingBtn = new Button("Continue Shopping");
		Button saveBtn = new Button("Save To Cart");
		Button exitBtn = new Button("Exit E-Bazaar");
		Text messageLabel = new Text();
		messageLabel.setFont(Font.font("Tahoma", FontWeight.MEDIUM, 12));
		messageLabel.setFill(Color.GREEN);
		
		table.setEditable(true);
		
		TableColumn item = new TableColumn("Item");
		item.setMinWidth(200);
		item.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("itemName"));
        
		TableColumn quantity = new TableColumn("Quantity");
		quantity.setMinWidth(100);
		quantity.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("quantity"));
		
		TableColumn unitPrice = new TableColumn("Unit Price");
		unitPrice.setMinWidth(100);
		unitPrice.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("unitPrice"));
		
		TableColumn total = new TableColumn("Total");
		total.setMinWidth(100);
		total.setCellValueFactory(new PropertyValueFactory<OrderLine, Double>("totalPrice"));
		
		TableColumn description = new TableColumn("Description");
		description.setMinWidth(300);
		description.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("description"));
		
		table.getColumns().addAll(item, quantity, unitPrice, total, description);
		
		table.setItems(this.observableOrderList);
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(10); 
		grid.setHgap(10);
		HBox titleBox = new HBox(10);
		titleBox.setAlignment(Pos.CENTER);
		titleBox.setPadding(new Insets(10));
		titleBox.getChildren().add(titleLabel);
		grid.add(titleBox, 0, 0);
		grid.add(table, 0, 1);
		
		HBox outputMessageBox = new HBox();
		outputMessageBox.setAlignment(Pos.CENTER);
		outputMessageBox.getChildren().add(messageLabel);
		grid.add(outputMessageBox, 0, 3);
		
		HBox btnBox = new HBox(10);
		btnBox.setAlignment(Pos.CENTER);
		btnBox.setSpacing(20);
		btnBox.setPadding(new Insets(15));
		btnBox.getChildren().add(proceedBtn);
		btnBox.getChildren().add(continueShoppingBtn);
		btnBox.getChildren().add(saveBtn);
		btnBox.getChildren().add(exitBtn);
		grid.add(btnBox,0,5);
		
		proceedBtn.setOnAction(evt-> {
			ShippingBilling shippingBillingWindow = new ShippingBilling(primaryStage, this.orderLineList, new Address());
			hide();
			shippingBillingWindow.show();
		});
		continueShoppingBtn.setOnAction(evt-> {
			CatalogListWindow catalogs = new CatalogListWindow(primaryStage, this.orderLineList);
			catalogs.setData(DefaultData.CATALOG_LIST_DATA);
			hide();
			catalogs.show();
		});
		saveBtn.setOnAction(evt -> {
			messageLabel.setText(this.orderLineList.size() + " item (s) successfully saved in your cart !!!");
		});
		exitBtn.setOnAction(evt -> {
			Platform.exit();
		});
		Scene scene = new Scene(grid,850, 600);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		setScene(scene);
	}
}

