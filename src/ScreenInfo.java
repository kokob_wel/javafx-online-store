import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class ScreenInfo {
	
	static Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
    public static double WIDTH = primScreenBounds.getWidth()*0.75;
    public static double HEIGHT = primScreenBounds.getHeight()*0.75;
}
