import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ShippingBilling extends Stage{

	private Stage primaryStage;
	private List<OrderLine> orderlineList;
	private Address addressOnRecord;
	
	public ShippingBilling(Stage ps, List<OrderLine> list, Address address){
		this.orderlineList = list;
		primaryStage = ps;
		this.addressOnRecord = address;
		primaryStage.setTitle("Shipping Billing");
		 
		 VBox vbMain = new VBox(10);
		 vbMain.setAlignment(Pos.CENTER);
		 vbMain.setPadding(new Insets(25, 25, 25, 25));
		 Label MAIN_LABEL = new Label("Shipping And Billing Information");
		 MAIN_LABEL.setFont(new Font("Arial", 23));
		 vbMain.getChildren().add(MAIN_LABEL);
		 
		 GridPane grid = new GridPane();
	     grid.setAlignment(Pos.CENTER);
	     grid.setHgap(10);
	     grid.setVgap(5);
	     grid.setPadding(new Insets(10, 10, 10, 10));
		 
	     VBox hbLTable = new VBox(10);
		 Label SHIP_LABEL = new Label("Shipping Address");
		 SHIP_LABEL.setAlignment(Pos.CENTER_LEFT);
		 hbLTable.getChildren().add(SHIP_LABEL);
		 
		 	GridPane shippingAddGrid = new GridPane();
		 	shippingAddGrid.setHgap(8);
		 	shippingAddGrid.setVgap(8);
		 	Label Name = new Label("Name ");
		 	shippingAddGrid.add(Name, 0, 1);

		 	TextField nameTextField = new TextField();
		 	shippingAddGrid.add(nameTextField, 1, 1);
		 	
		 	Label address1 = new Label("Address 1 ");
		 	shippingAddGrid.add(address1, 0, 2);

		 	TextField address1TextField = new TextField(addressOnRecord.getStreet());
		 	shippingAddGrid.add(address1TextField, 1, 2);
		 	
		 	Label city = new Label("City ");
		 	shippingAddGrid.add(city, 0, 3);

		 	TextField cityTextField = new TextField(addressOnRecord.getCity());
		 	shippingAddGrid.add(cityTextField, 1, 3);
		 	
		 	Label state = new Label("State ");
		 	shippingAddGrid.add(state, 0, 4);

		 	TextField stateTextField = new TextField(addressOnRecord.getState());
		 	shippingAddGrid.add(stateTextField, 1, 4);	
		 	
		 	Label zip = new Label("Zip ");
		 	shippingAddGrid.add(zip, 0, 5);

		 	TextField zipTextField = new TextField(addressOnRecord.getZip());
		 	shippingAddGrid.add(zipTextField, 1, 5);
		 	
		 hbLTable.getChildren().add(shippingAddGrid);
		 
		 grid.add(hbLTable, 0, 1);
		 
		 VBox hbRTable = new VBox(10);
		 Label BILL_LABEL = new Label("Billing Address");
		 BILL_LABEL.setAlignment(Pos.CENTER_LEFT);
		 hbRTable.getChildren().add(BILL_LABEL);
		 
		 GridPane billingAddGrid = new GridPane();
		 	billingAddGrid.setHgap(8);
		 	billingAddGrid.setVgap(8);
		 	Label Name2 = new Label("Name ");
		 	billingAddGrid.add(Name2, 0, 1);

		 	TextField nameTextField2 = new TextField();
		 	billingAddGrid.add(nameTextField2, 1, 1);
		 	
		 	Label address12 = new Label("Address 1 ");
		 	billingAddGrid.add(address12, 0, 2);

		 	TextField address1TextField2 = new TextField();
		 	billingAddGrid.add(address1TextField2, 1, 2);
		 	
		 	Label city2 = new Label("City ");
		 	billingAddGrid.add(city2, 0, 3);

		 	TextField cityTextField2 = new TextField();
		 	billingAddGrid.add(cityTextField2, 1, 3);
		 	
		 	Label state2 = new Label("State ");
		 	billingAddGrid.add(state2, 0, 4);

		 	TextField stateTextField2 = new TextField();
		 	billingAddGrid.add(stateTextField2, 1, 4);	
		 	
		 	Label zip2 = new Label("Zip ");
		 	billingAddGrid.add(zip2, 0, 5);

		 	TextField zipTextField2 = new TextField();
		 	billingAddGrid.add(zipTextField2, 1, 5);

		 hbRTable.getChildren().add(billingAddGrid);
		 
		 grid.add(hbRTable, 1, 1);
		 
		 vbMain.getChildren().add(grid);
		 
		 CheckBox addressesSameCheckbox = new CheckBox("Check if Billing Address is Sam as Shipping");
		 vbMain.getChildren().add(addressesSameCheckbox);
		 
		 Label SHIPPING_METHOD = new Label("Shipping Method");
		 
		 //VBox shipMethodRadioBtns = new VBox(10);
		 GridPane shipMethodRadioBtns = new GridPane();
		 shipMethodRadioBtns.setAlignment(Pos.CENTER);
		 
		 ToggleGroup group1 = new ToggleGroup();
		 
		 RadioButton shipMethods1 = new RadioButton("Pigeon-carrier Ground");
		 shipMethods1.setToggleGroup(group1);
		 shipMethods1.setPadding(new Insets(5));
		 
		 RadioButton shipMethods2 = new RadioButton("Pigeon-carrier Air");
		 shipMethods2.setToggleGroup(group1);
		 shipMethods2.setPadding(new Insets(5));
		 
		 RadioButton shipMethods3 = new RadioButton("Pigeon-carrier Overnight");
		 shipMethods3.setToggleGroup(group1);
		 shipMethods3.setPadding(new Insets(5));
		 
		 shipMethodRadioBtns.add(shipMethods1, 0, 1);
		 shipMethodRadioBtns.add(shipMethods2, 0, 2);
		 shipMethodRadioBtns.add(shipMethods3, 0, 3);
		 
		 vbMain.getChildren().add(SHIPPING_METHOD);
		 vbMain.getChildren().add(shipMethodRadioBtns);
		 
		 HBox bottomButtons = new HBox();
		 bottomButtons.setAlignment(Pos.BOTTOM_CENTER);
		 bottomButtons.setSpacing(10);
		 
		 Button selectShipAdd = new Button("Select Shipping Address");
		 selectShipAdd.setEffect(new Reflection());

		 Button proceedWithChkout = new Button("Proceed With Checkout");
		 proceedWithChkout.setEffect(new Reflection());

		 Button backToCart = new Button("Back To Cart");
		 backToCart.setEffect(new Reflection());

		 bottomButtons.getChildren().add(selectShipAdd);
		 bottomButtons.getChildren().add(proceedWithChkout);
		 bottomButtons.getChildren().add(backToCart);
		 
		 vbMain.getChildren().add(bottomButtons);
		 
		 selectShipAdd.setOnAction(evt -> {
		  AddressesWindow addressesWin = new AddressesWindow(primaryStage, this.orderlineList);
			 hide();
			 addressesWin.show();
			 
		 });
		 
		 proceedWithChkout.setOnAction(evt -> {
				PaymentWindow paymentWin = new PaymentWindow(primaryStage , this.orderlineList);
				hide();
				paymentWin.show();
			});
		
		 backToCart.setOnAction(evt -> {
			 CartItemsWindow cartItemsWindow = new CartItemsWindow(primaryStage, this.orderlineList);
			 hide();
			 cartItemsWindow.show();
		 });
		 
		 addressesSameCheckbox.setOnAction(evt -> {
			if(addressesSameCheckbox.isSelected()){
				address1TextField2.setText(address1TextField.getText());
				cityTextField2.setText(cityTextField.getText());
				stateTextField2.setText(stateTextField.getText());
				zipTextField2.setText(zipTextField.getText());
//				System.out.println("CheckBox Action (selected: ") ;
			 }
			else{address1TextField2.clear();
			cityTextField2.clear();
			stateTextField2.clear();
			zipTextField2.clear();}
			 
		 });
		 
		 Scene scene = new Scene(vbMain,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		 scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
	     setScene(scene);
	}
}
