import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import static javafx.application.Application.launch;


public class Start extends Stage {
	
	
	private Stage primaryStage;
	private List<OrderLine> orderLineList  = new ArrayList<OrderLine>();
	
//	public static void main(String[] args) {
//		launch(args);
//	}

	public Start(Stage ps) {
			primaryStage = ps;
			primaryStage.setTitle("Welcome Page");
				
		VBox topContainer = new VBox();
		MenuBar mainMenu = new MenuBar();
		Text label = new Text("\n     Welcome     \n     To Our       \n EBazar Shop");
		label.setFont(Font.font("Comic Sans MS", FontWeight.BOLD, 60));
		label.setFill(Color.DARKRED);
	
		
		HBox labelBox = new HBox(10);
		labelBox.setAlignment(Pos.CENTER);
		labelBox.getChildren().add(label);
		
		
		topContainer.getChildren().add(mainMenu);
		topContainer.getChildren().add(labelBox);

		Menu custMenu = new Menu("Customer");
		MenuItem onlinePurchase = new MenuItem("Online Purchase");
		
		onlinePurchase.setOnAction(evt -> {	
			CatalogListWindow catalogs = new CatalogListWindow(primaryStage,orderLineList);
	        catalogs.setData(DefaultData.CATALOG_LIST_DATA);
	        catalogs.show();  
	        primaryStage.hide();
					
		});
		
		//review orders menu item
		MenuItem menuItemRevOrders =  new MenuItem("Review Orders");
		menuItemRevOrders.setOnAction(evt -> {	
			ReviewOrder reviews = new ReviewOrder(primaryStage);
			reviews.setData(DefaultData.orders);
			reviews.show();		
	        primaryStage.hide();
					
		});
		
		//retrieve cart
		MenuItem menuItemRetrieveSavedCart =  new MenuItem("Retrieve Saved Cart");
		menuItemRetrieveSavedCart.setOnAction(evt -> {	
			
			List<OrderLine> savedCart = new ArrayList<OrderLine>();
			savedCart.add(new OrderLine("T-Shirts",2,52.00,"Can be worn by men or women. \nAlways in style."));
			savedCart.add(new OrderLine("Messiah of Dune",1,43.00,"You saw how good Dune was. \nThis is Part 2 of this \nunforgettable trilogy."));
			savedCart.add(new OrderLine("pants",3,12.00,"I've seen the Grand Canyon. \nI've camped at Yellowstone. But nothing \non earth compares to the look and feel of \nthese pants."));
			SavedCartWindow savedCartWin = new SavedCartWindow(primaryStage,savedCart);
			
			savedCartWin.show();  
	        primaryStage.hide();
					
		});		
		
		MenuItem exitApp = new MenuItem("Exit");
		exitApp.setOnAction(evt -> Platform.exit());
			
		custMenu.getItems().addAll(onlinePurchase, menuItemRevOrders, menuItemRetrieveSavedCart,exitApp);

		//did not add menu item for menu edit
		Menu adminMenu = new Menu("Administrator");
		
		MenuItem menuItemMaintainProduct = new MenuItem("Maintain Product Catalog");		
		menuItemMaintainProduct.setOnAction(e->{
			MaintainProductCatalog catalogs = new MaintainProductCatalog(primaryStage);
			catalogs.setBookData(DefaultData.Books);
			catalogs.setClothesData(DefaultData.clothes);
			catalogs.show();		
	        primaryStage.hide();
			
		});
		
		
		MenuItem menuItemMaintainCatalogTypes = new MenuItem("Maintain Catalog Types");	
		menuItemMaintainCatalogTypes.setOnAction(e->{
			MaintainCatalogTypes catalogTypes = new MaintainCatalogTypes(primaryStage);			
			catalogTypes.show();		
	        primaryStage.hide();
			
		});
		adminMenu.getItems().addAll(menuItemMaintainProduct,menuItemMaintainCatalogTypes);
		
		
		mainMenu.getMenus().addAll(custMenu, adminMenu);	
		
		
		Scene scene = new Scene(topContainer,ScreenInfo.WIDTH,ScreenInfo.HEIGHT);
		primaryStage.setScene(scene);
		scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		
		primaryStage.show();
	}
	
}
