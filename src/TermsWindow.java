import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TermsWindow extends Stage{
	
 private Stage primaryStage;
 private List<OrderLine> orderlineList;
 
	public TermsWindow(Stage ps , List<OrderLine> list) {
		this.primaryStage = ps;
		this.orderlineList = list;
		primaryStage.setTitle("Terms and Conditions");
		 
		VBox vbMain = new VBox(20);
		 
		 vbMain.setAlignment(Pos.BOTTOM_CENTER);
		 vbMain.setPadding(new Insets(25, 25, 25, 25));
		 Label MAIN_LABEL = new Label("Terms and Conditions");
		 vbMain.getChildren().add(MAIN_LABEL);
		 
		 TextArea termsTextField = new TextArea();
		 
		    termsTextField.setPrefRowCount(10);
	        termsTextField.setPrefColumnCount(100);
	        termsTextField.setWrapText(true);
	        termsTextField.setPrefWidth(150);
	      

	       final String termsDefault = "Any Items purchased from this site adhere to the terms and "+
					  "conditions depicted in this document. You will have to accecpt "+
					  "the Terms and Conditions depicted here inorder to purchase " +
					  "anything from this site.";
	        
	        termsTextField.setText(termsDefault);
	        
//	        Text termsText = new Text();
//	        termsText.setX(150);
//	        termsText.setY(100);
//	        termsText.setText("Any Items purchased from this site adhere to the terms and "+
//					  "conditions depicted in this document. You will have to accecpt "+
//					  "the Terms and Conditions depicted here inorder to purchase " +
//					  "anything from this site.");
		  	
		 vbMain.getChildren().add(termsTextField);
//		 vbMain.getChildren().add(termsText);
		 
		 HBox bottomButtons = new HBox();
		 
		 bottomButtons.setAlignment(Pos.BOTTOM_CENTER);
		 bottomButtons.setSpacing(10);
		 
		 Button acceptTerms = new Button("Accept Terms and Conditions");
		 acceptTerms.setEffect(new Reflection());

		 Button exitEbazar = new Button("Exit E-Bazar");
		 exitEbazar.setEffect(new Reflection());
		 
		 bottomButtons.getChildren().add(acceptTerms);
		 bottomButtons.getChildren().add(exitEbazar);
		 
		 vbMain.getChildren().add(bottomButtons);
	     
		 acceptTerms.setOnAction(evt -> {
			    FinalizeOrderWindow finalOrder = new FinalizeOrderWindow(primaryStage, orderlineList);
				hide();			
				finalOrder.show();
			});
		 
		 exitEbazar.setOnAction(evt -> Platform.exit());
		 
		 Scene scene = new Scene(vbMain,500,250);
		 scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		 setScene(scene);
	}
}