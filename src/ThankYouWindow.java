import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ThankYouWindow extends Stage {

	private Stage primaryStage;
	
	public ThankYouWindow(Stage ps){
		this.primaryStage = ps;
	
		primaryStage.setTitle("thank youu window");

		VBox vbMain = new VBox(20);
		 
		 vbMain.setAlignment(Pos.BOTTOM_CENTER);
		 vbMain.setPadding(new Insets(25, 25, 25, 25));
		 Label MAIN_LABEL = new Label("Thank you for shopping with us");
		 vbMain.getChildren().add(MAIN_LABEL);
		 
		 Text thankyoutext = new Text();
		 
		 thankyoutext.setText( "Thank You for Shopping at the Ebazaar. "+
        	             "We guarantee satisfaction and quality for our product.");
		 vbMain.getChildren().add(thankyoutext);
		 
		 Button exitEbazar = new Button("Exit E-Bazar");
		 exitEbazar.setEffect(new Reflection());
		 
		 vbMain.getChildren().add(exitEbazar);
		 
         exitEbazar.setOnAction(evt -> Platform.exit());
		 
		 Scene scene = new Scene(vbMain,600,150);
		 scene.getStylesheets().add(getClass().getResource("gui.css").toExternalForm());
		 setScene(scene);
	}
}
